package proect_dom_parsing;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;

import org.json.simple.JSONObject;

public class ListAuto {

	public static void main(String[] args) throws IOException {
		String FILENAME= "C:\\Users\\�����\\Desktop\\inform.json";// ����� ���� ������� ���� resultJson.json � ������� � ���� ���� �� ����� ��
		File file = new File(FILENAME);
		if (!file.exists()){
	        throw new FileNotFoundException(file.getName());
	    }
    	
    	for(int i=1;i<=10;i++){
    		Document doc = Jsoup.connect("http://auto.drom.ru/page2/?go_search="+i).get();
    		Elements catalog_jsElements = doc.getElementsByAttributeValue("class", "c_n f14 b-tableCell");

    		catalog_jsElements.forEach(catalog_jsElement -> {	
    			Element aElement = catalog_jsElement.child(0);
    			String url =aElement.attr("href");
    			String title = aElement.text();
    			//aElemenSystem.out.println(url+ '\n' + title);
    			JSONObject resultJson = new JSONObject();
    			resultJson.put("name", title);
    			resultJson.put("url", url);
    			//System.out.println(resultJson.toString());
    			try {
    				update(file,resultJson.toString());
    			} catch (FileNotFoundException e) {
    				e.printStackTrace();
    			}
    		});
    		
    	}
    }
	public static void write(File file, String text) {
	    try {
	        if(!file.exists()){
	            file.createNewFile();
	        }
	        PrintWriter out = new PrintWriter(file.getAbsoluteFile());
	        
	        try {
	            out.print(text);
	        } finally {
	            out.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }
	}
	public static String read(File file) throws FileNotFoundException {
	    StringBuilder sb = new StringBuilder();
	 
	    try {
	        BufferedReader in = new BufferedReader(new FileReader( file.getAbsoluteFile()));
	        try {
	            String s;
	            while ((s = in.readLine()) != null) {
	                sb.append(s);
	                sb.append("\n");
	            }
	        } finally {
	            in.close();
	        }
	    } catch(IOException e) {
	        throw new RuntimeException(e);
	    }

	    return sb.toString();
	}
	public static void update(File file, String newText) throws FileNotFoundException {
	    StringBuilder sb = new StringBuilder();
	    String oldFile = read(file);
	    sb.append(oldFile);
	    sb.append(newText);
	    write(file, sb.toString());
	}
}